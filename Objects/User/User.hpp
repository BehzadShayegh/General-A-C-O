#ifndef USER_H
#define USER_H

#include <iostream>
#include <vector>
#include <string>
#include <fstream>
using namespace std;

class User{
public:

    enum Person_type {
        PRIVATE,
        USUAL_LEGAL,
        GOVERMENTAL_LEGAL
    };

    enum Nationality {
        IRAN,
        OTHER
    };

	User(int unique_code, string name);

    void edit_personal_info(
        string economical_code = "",
        string telephone_number = "",
        string mobile_number = "",
        string work_city = "",
        string email = "",
        string birth_date = "",
        string meli_code = "",
        string post_code = "",
        string web_page = "",
        string job = "",
        string address = "",
        string name = "",
        string signup_date = ""
    );

    void edit_financial_info(
        int reduction_percentage = 0,
        int reductions_sum = 0,
        int credit = 0,
        int reckoning = 0
    );

    void edit_account_info(
        bool buyer = false,
        bool seller = false,
        bool middleman = false,
        bool black_list = false,
        string middleman_name = "",
        int commission_percentage = 0,
        Person_type person_type = PRIVATE,
        Nationality nationality = IRAN
    );

    void edit_banking_info(
        string company_name = "",
        string info = ""
    );

    void save_in_file();
	
private:
    int unique_code;

    struct Personal_info {
        string economical_code;
        string telephone_number;
        string mobile_number;
        string work_city;
        string email;
        string birth_date;
        string meli_code;
        string post_code;
        string web_page;
        string job;
        string address;
        string name;
        string signup_date;
    } personal_info;
    
    struct Financial_info {
        int reduction_percentage;
        int reductions_sum;
        int credit;
        int reckoning;
    } financial_info;

    struct Account_info {
        bool buyer;
        bool seller;
        bool middleman;
        bool black_list;
        string middleman_name;
        int commission_percentage;
        Person_type person_type;
        Nationality nationality;
    } account_info;

    struct Banking_info {
        string company_name;
        string info;
    } banking_info;

    string convert_to_string();
};

#endif