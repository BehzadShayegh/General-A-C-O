#include "User.hpp"

using namespace std;

int main(){
    string line;
    int num;
    cin >> line >> num;
    User user(num, line);
    user.edit_account_info(true);
    user.edit_financial_info(100, 200, 300, 400);
    user.save_in_file();
    return 0;
}
