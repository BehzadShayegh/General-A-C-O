#include "User.hpp"

User::User(int uc, string n) {
    unique_code = uc;
    edit_personal_info();
    edit_financial_info();
    edit_account_info();
    edit_banking_info();
    personal_info.name = n;
}

void User::edit_personal_info(
    string economical_code,
    string telephone_number,
    string mobile_number,
    string work_city,
    string email,
    string birth_date,
    string meli_code,
    string post_code,
    string web_page,
    string job,
    string address,
    string name,
    string signup_date
) {
    personal_info.economical_code = economical_code;
    personal_info.telephone_number = telephone_number;
    personal_info.mobile_number = mobile_number;
    personal_info.work_city = work_city;
    personal_info.email = email;
    personal_info.birth_date = birth_date;
    personal_info.meli_code = meli_code;
    personal_info.post_code = post_code;
    personal_info.web_page = web_page;
    personal_info.job = job;
    personal_info.address = address;
    personal_info.name = name;
    personal_info.signup_date = signup_date;
}

void User::edit_financial_info(
    int reduction_percentage,
    int reductions_sum,
    int credit,
    int reckoning
) {
    financial_info.reduction_percentage = reduction_percentage;
    financial_info.reductions_sum = reductions_sum;
    financial_info.credit = credit;
    financial_info.reckoning = reckoning;
}

void User::edit_account_info(
    bool buyer,
    bool seller,
    bool middleman,
    bool black_list,
    string middleman_name,
    int commission_percentage,
    Person_type person_type,
    Nationality nationality
) {
    account_info.buyer = false;
    account_info.seller = false;
    account_info.middleman = false;
    account_info.black_list = false;
    account_info.middleman_name = "";
    account_info.commission_percentage = 0;
    account_info.person_type = PRIVATE;
    account_info.nationality = IRAN;
}

void User::edit_banking_info(
    string company_name,
    string info
) {
    banking_info.company_name = "";
    banking_info.info = "";
}

string User::convert_to_string() {
    string str = "";
    str += to_string(unique_code) + '\n';
    str += '\n';
    str += "Personal_info:\n";
    str += personal_info.economical_code + '\n';
    str += personal_info.telephone_number + '\n';
    str += personal_info.mobile_number + '\n';
    str += personal_info.work_city + '\n';
    str += personal_info.email + '\n';
    str += personal_info.birth_date + '\n';
    str += personal_info.meli_code + '\n';
    str += personal_info.post_code + '\n';
    str += personal_info.web_page + '\n';
    str += personal_info.job + '\n';
    str += personal_info.address + '\n';
    str += personal_info.name + '\n';
    str += personal_info.signup_date + '\n';
    str += '\n';
    str += "Financial_info:\n";
    str += to_string(financial_info.reduction_percentage) + '\n';
    str += to_string(financial_info.reductions_sum) + '\n';
    str += to_string(financial_info.credit) + '\n';
    str += to_string(financial_info.reckoning) + '\n';
    str += '\n';
    str += "Account_info:\n";
    str += account_info.buyer ? "true\n" : "false\n";
    str += account_info.seller ? "true\n" : "false\n";
    str += account_info.middleman ? "true\n" : "false\n";
    str += account_info.black_list ? "true\n" : "false\n";
    str += account_info.middleman_name + '\n';
    str += to_string(account_info.commission_percentage) + '\n';
    str += account_info.person_type == PRIVATE ? "private\n" :
            account_info.person_type == USUAL_LEGAL ? "usual_legal\n" :
            account_info.person_type == GOVERMENTAL_LEGAL ? "govermental_legal\n" :
            "\n";
    str += account_info.nationality == IRAN ? "Iran\n" : "other\n";
    str += '\n';
    str += "Banking_info:\n";
    str += banking_info.company_name + '\n';
    str += banking_info.info + '\n';
    str += '\n';
    str += "__FINISH__";

    return str;
}

void User::save_in_file() {
    ofstream file;
    file.open("./users_info/"+to_string(unique_code));
    file << this->convert_to_string();
    file.close();
}